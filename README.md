# CECyTE-Plantel-25

Sistema Web para el control de Inventario y Préstamo de libros del Centro de Información del CECyTE Plantel 25 “San Pablo Huixtepec”

## Sinopsis

Este sistema permite la automatización de los procesos de control de inventario, y préstamo de libros, tanto físicos como digitales, a través de una interfaz Web, gestionada con Java e interconectada con una base de datos en MySQL. 
Es importante recalcar que, con la implementación del sistema se optimiza el control y gestión del inventario del Centro de Información, con el objetivo de brindar información oportuna a todos los estudiantes y docentes. Se puede obtener un listado de todos los libros en stock, el número de ejemplares con los que se cuenta, las editoriales y buscar en cualquier momento algún material bibliográfico bajo distintas reglas de filtrado, por ejemplo, se poden realizar búsquedas por título del libro, autor, editorial o ISBN.

# Presentación de las UI

[Interfaces gráficas del sistema](https://drive.google.com/file/d/16UMb0Yqg-Z7EkQqjZPA4Ul0OFxkz1Dr-/view?usp=sharing)

## Autor

Jhonatan Barroso García
