/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Libro;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class LibroFacade extends AbstractFacade<Libro> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LibroFacade() {
        super(Libro.class);
    }
    public List<Libro> find(String search){
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        return em.createQuery(
        "SELECT l FROM Libro l WHERE l.id = ?1 OR l.titulo LIKE ?2 OR l.idAutor.autor LIKE ?3 OR l.idEditorial.editorial LIKE ?4")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%")
        .getResultList();
    }
 
    public List<Libro> findRange(int[] range, String search) {
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        javax.persistence.Query q = em.createQuery(
        "SELECT l FROM Libro l WHERE l.id = ?1 OR l.titulo LIKE ?2 OR l.idAutor.autor LIKE ?3 OR l.idEditorial.editorial LIKE ?4")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .setParameter(3, "%"+search+"%")
        .setParameter(4, "%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
}
