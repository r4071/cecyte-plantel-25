/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Prestamo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class PrestamoFacade extends AbstractFacade<Prestamo> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PrestamoFacade() {
        super(Prestamo.class);
    }
    public List<Prestamo> find(String search){
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        return em.createQuery(
        "SELECT p FROM Prestamo p WHERE p.id = ?1 OR p.idLibro.titulo LIKE ?2")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .getResultList();
    }
 
    public List<Prestamo> findRange(int[] range, String search) {
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        javax.persistence.Query q = em.createQuery(
        "SELECT p FROM Prestamo p WHERE p.id = ?1 OR p.idLibro.titulo LIKE ?2")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
}
