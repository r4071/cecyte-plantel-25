/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Solicitud;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class SolicitudFacade extends AbstractFacade<Solicitud> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SolicitudFacade() {
        super(Solicitud.class);
    }
    
    public List<Solicitud> find(String search){
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        return em.createQuery(
        "SELECT s FROM Solicitud s WHERE s.id = ?1 OR s.idLibro.titulo LIKE ?2")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%")
        .getResultList();
    }
 
    public List<Solicitud> findRange(int[] range, String search) {
        int id = -1;
        try{
            id=Integer.parseInt(search);
        }catch(NumberFormatException e){}
        javax.persistence.Query q = em.createQuery(
        "SELECT s FROM Solicitud s WHERE s.id = ?1 OR s.idLibro.titulo LIKE ?2")
        .setParameter(1,id)
        .setParameter(2, "%"+search+"%");
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
}
