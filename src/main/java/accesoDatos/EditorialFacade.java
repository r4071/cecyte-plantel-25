/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accesoDatos;

import entidades.Editorial;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author JHONATAN
 */
@Stateless
public class EditorialFacade extends AbstractFacade<Editorial> {

    @PersistenceContext(unitName = "my_persistence_unit")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EditorialFacade() {
        super(Editorial.class);
    }
    public List<Editorial> find(String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<Editorial> rt = cq.from(Editorial.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("editorial"), "%"+search+"%")
        ));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return q.getResultList();
    }
    public List<Editorial> findRange(int[] range, String search) {
        int id = -1;
        try{
            id = Integer.parseInt(search);
        }catch(NumberFormatException e){}
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<Editorial> rt = cq.from(Editorial.class);
        cq.select(rt);
        cq.where(getEntityManager().getCriteriaBuilder().or(
            getEntityManager().getCriteriaBuilder().equal(rt.get("id"), id),
            getEntityManager().getCriteriaBuilder().like(rt.get("editorial"), "%"+search+"%")
        ));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }
    
}
