/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import accesoDatos.DocenteFacade;
import accesoDatos.EstudianteFacade;
import accesoDatos.UsuariosFacade;
import controlador.util.JsfUtil;
import entidades.Docente;
import entidades.Estudiante;
import entidades.Usuarios;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

/**
 *
 * @author JHONATAN
 */
@Named
@SessionScoped
public class Logueo implements Serializable {

    @EJB
    private UsuariosFacade usuariosFacade;

    @EJB
    private EstudianteFacade estudianteFacade;

    @EJB
    private DocenteFacade docenteFacade;
    
    private String usuario;
    private String password;
    private boolean modo; //Activar o desactivar modo oscuro
    private Usuarios userSystem;
    private Estudiante alumno;
    private Docente docente;
    
    

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    private Docente buscarDocente(){
        try{
        int mat = Integer.parseInt(usuario);
        docente = docenteFacade.find(mat);
        }catch(NumberFormatException e){}
        return docente;
    }
    
    private Estudiante buscarAlumno(){
        try{
        alumno = estudianteFacade.buscar(usuario, password).get(0);
        }catch(Exception e){}
        return alumno;
    }
    private Usuarios buscarUsuario(){
        try{
        userSystem = usuariosFacade.buscar(usuario, password).get(0);
        }catch(Exception e){}
        return userSystem;
    }
    public Usuarios getUserA(){
        return buscarUsuario();
    }
    
    public String validarUsuario(){
        String page="";
        if(buscarUsuario() != null && userSystem.getStatus() == 1 ){
            page="index.xhtml";
            HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", usuario);
        }else if(buscarDocente() != null && docente.getPassword().equals(password) ){
            page="publico.xhtml";
            HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", usuario);
        }else if(buscarAlumno() != null){
            page="publico.xhtml";
            HttpSession session = SessionUtils.getSession();
			session.setAttribute("username", usuario);
        }else{
            JsfUtil.error("Acceso Denegado.!!");
        }
        
        return page;
    }
    
    public String logout() {
	HttpSession session = SessionUtils.getSession();
	session.invalidate();
	return "/login.xhtml";
    }
    
    public String getDatosU(){
        String datos = "";
        if(userSystem != null)
           datos = userSystem.getNombre();
        else if(alumno != null)
           datos = alumno.getNombre()+" "+alumno.getApellidoP();
        else if(docente != null)
           datos = docente.getNombre()+" "+docente.getApellidoP();     
        return datos;
    }

    public Boolean getModo() {
        return modo;
    }

    public void setModo(Boolean modo) {
        this.modo = modo;
    }
    public String cambiarModoLT1(){
        String templante = "";
        if(!modo)
            templante = "/template.xhtml";
        else
            templante = "/templateL.xhtml";
         return templante;   
    }
    public String cambiarModoLT3(){
        String templante = "";
        if(!modo)
            templante = "/template3.xhtml";
        else
            templante = "/template3L.xhtml";
        System.out.println("Modo "+modo);
        System.out.println("Template "+templante);
         return templante;   
    }

    
    public String activarModoClaro(){
        modo = true;
        return "/index.xhtml";
    }
    public String activarModoOscuro(){
        modo = false;
        return "/index.xhtml";
    }
    
    
}
