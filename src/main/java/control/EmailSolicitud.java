package control;

/**
 *
 * @author JHONATAN
 */
public class EmailSolicitud {
    private String idSolicitante;
    private String nombreSolicitante;
    private String tipo;
    private String fecha;
    private int idLibro;
    private String titulo;
    private String editorial;
    private String autor;
    private String comentarios;

    public EmailSolicitud(String idSolicitante, String nombreSolicitante, String tipo, String fecha, int idLibro, String titulo, String editorial, String autor, String comentarios) {
        this.idSolicitante = idSolicitante;
        this.nombreSolicitante = nombreSolicitante;
        this.tipo = tipo;
        this.fecha = fecha;
        this.idLibro = idLibro;
        this.titulo = titulo;
        this.editorial = editorial;
        this.autor = autor;
        this.comentarios = comentarios;
    }

    public String getIdSolicitante() {
        return idSolicitante;
    }

    public void setIdSolicitante(String idSolicitante) {
        this.idSolicitante = idSolicitante;
    }

    public String getNombreSolicitante() {
        return nombreSolicitante;
    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getIdLibro() {
        return idLibro;
    }

    public void setIdLibro(int idLibro) {
        this.idLibro = idLibro;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
    
    public String generarHTMLSolicitud(){
        String mail = "<html><head></head><body><div style='background-color:#f9f9f9'>\n" +
                        "      <div style='margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:0px;text-align:center'>\n" +
                        "\n" +
                        "      <div style='background:transparent;background-color:transparent;margin:0px auto;max-width:604px'>    \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:transparent;background-color:transparent;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:20px 0px;text-align:center'>\n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      <div style='margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:0;text-align:center'>\n" +
                        "                \n" +
                        "    \n" +
                        "      \n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:8px;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%;border-radius:8px'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:20px 20px 0;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-100' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                \n" +
                        "              </tr>\n" +
                        "            \n" +
                        "              <tr>\n" +
                        "                <td align='center' style='font-size:0px;padding:12px;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <div style='font-family:Poppins,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:20px;font-weight:bold;line-height:1;text-align:center;color:#000000'>FAWARIS BOOK APP</div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "          \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      \n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:12px 24px;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-100' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td align='center' style='font-size:0px;padding:0;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <p style='border-top:solid 1px #f0f2f4;font-size:1px;margin:0px auto;width:100%'>\n" +
                        "      </p>\n" +
                        "\n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "          \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:12px 24px;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      \n" +
                        "    \n" +
                        "          \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-66' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          <tr>\n" +
                        "            <td style='vertical-align:middle;padding:10px 16px'>\n" +
                        "              \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td align='center' style='font-size:0px;padding:0;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <div style='font-family:Poppins,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:18px;font-weight:500;line-height:1;text-align:center;color:#000000'>Solicitud de Material Bibliográfico</div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "\n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:8px;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%;border-radius:8px'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:20px;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      \n" +
                        "    \n" +
                        "          \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-70' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:middle' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td align='left' style='font-size:0px;padding:20px 20px 0;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <div style='font-family:Poppins,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:18px;font-weight:700;line-height:23px;text-align:left;color:#000000'>Datos de la Solicitud:</div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "              <tr>\n" +
                        "                <td align='left' style='font-size:0px;padding:0px 20px 0px 20px;word-break:break-word'>\n" +
                        "                  <br/>\n" +
                        "      <div style='font-family:Poppins,Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;font-size:14px;line-height:20px;text-align:left;color:#505464!important'><p><strong>ID del Solicitante: </strong>"+idSolicitante+"</p>\n" +
                        "<p><strong>Nombre del Solicitante: </strong>"+nombreSolicitante+"</p><p><strong>Tipo: </strong>"+tipo+"</p><p><strong>Fecha de Solicitud: </strong>"+fecha+"</p><p><strong>Clave del Libro: </strong>"+idLibro+"</p><p><strong>Título del Libro: </strong>"+titulo+"</p><p><strong>Editorial: </strong>"+editorial+"</p><p><strong>Autor: </strong>"+autor+"</p><p><strong>Comentarios: </strong>"+comentarios+"</p></div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "              \n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "          \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div></div>\n" +
                        "    \n" +
                        "      \n" +
                        "      \n" +
                        "        \n" +
                        "          \n" +
                        "    \n" +
                        "      \n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:12px 24px;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-100' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td align='center' style='font-size:0px;padding:0;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <p style='border-top:solid 1px #f0f2f4;font-size:1px;margin:0px auto;width:100%'>\n" +
                        "      </p>\n" +
                        "      \n" +
                        "\n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "          \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      <div style='background:transparent;background-color:transparent;margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:transparent;background-color:transparent;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:0;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-100' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td style='font-size:0px;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <div style='height:20px;line-height:20px'> </div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "\n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      <div style='background:#ffffff;background-color:#ffffff;margin:0px auto;border-radius:8px;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#ffffff;background-color:#ffffff;width:100%;border-radius:8px'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:20px;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-30' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:middle;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:middle' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td align='center' style='font-size:0px;padding:0;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='border-collapse:collapse;border-spacing:0px'>\n" +
                        "        <tbody>\n" +
                        "          <tr>\n" +
                        "            <td style='width:151px'>\n" +
                        "              \n" +
                        "      <img alt='Logo' height='auto' src='http://blog.connext.es/hubfs/blog-files/auditoria_SEO_imagenes/libros-blancos.png' style='border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px' width='151' class='CToWUd'>\n" +
                        "    \n" +
                        "            </td>\n" +
                        "          </tr>\n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "\n" +
                        "      <div style='background:transparent;background-color:transparent;margin:0px auto;max-width:604px'>\n" +
                        "        \n" +
                        "        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:transparent;background-color:transparent;width:100%'>\n" +
                        "          <tbody>\n" +
                        "            <tr>\n" +
                        "              <td style='direction:ltr;font-size:0px;padding:0;text-align:center'>\n" +
                        "                \n" +
                        "            \n" +
                        "      <div class='m_6628141547610389190mj-column-per-100' style='font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%'>\n" +
                        "        \n" +
                        "      <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top' width='100%'>\n" +
                        "        <tbody>\n" +
                        "          \n" +
                        "              <tr>\n" +
                        "                <td style='font-size:0px;word-break:break-word'>\n" +
                        "                  \n" +
                        "      <div style='height:20px;line-height:20px'> </div>\n" +
                        "    \n" +
                        "                </td>\n" +
                        "              </tr>\n" +
                        "            \n" +
                        "        </tbody>\n" +
                        "      </table>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "    \n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "    \n" +
                        "\n" +
                        "              </td>\n" +
                        "            </tr>\n" +
                        "          </tbody>\n" +
                        "        </table>\n" +
                        "        \n" +
                        "      </div>\n" +
                        "    \n" +
                        "      </div>\n" +
                        "</body></html>";
        return mail;
    }
    
    
    
}
