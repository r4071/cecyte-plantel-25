/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

/**
 *
 * @author JHONATAN
 */
public class Apartado {
    private String enlace;
    private String nombre;
    private String indicaciones;
    private String palabrasC;

    public Apartado() {
    }

    public Apartado(String enlace, String nombre, String indicaciones, String palabrasC) {
        this.enlace = enlace;
        this.nombre = nombre;
        this.indicaciones = indicaciones;
        this.palabrasC = palabrasC;
    }


    public String getEnlace() {
        return enlace;
    }

    public void setEnlace(String enlace) {
        this.enlace = enlace;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIndicaciones() {
        return indicaciones;
    }

    public void setIndicaciones(String indicaciones) {
        this.indicaciones = indicaciones;
    }

    public String getPalabrasC() {
        return palabrasC;
    }

    public void setPalabrasC(String palabrasC) {
        this.palabrasC = palabrasC;
    }
    
    
}
