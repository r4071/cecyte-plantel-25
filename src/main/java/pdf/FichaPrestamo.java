/*
 * Copyright (c) 2021, JHONATAN
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package pdf;

import com.koadweb.javafpdf.Alignment;
import com.koadweb.javafpdf.Borders;
import com.koadweb.javafpdf.FPDF;
import com.koadweb.javafpdf.FontStyle;
import com.koadweb.javafpdf.Format;
import com.koadweb.javafpdf.Orientation;
import com.koadweb.javafpdf.Position;
import entidades.Prestamo;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JHONATAN
 */
public class FichaPrestamo extends FPDF {
    private List<Prestamo> librosSeleccionados;
    private String matricula; // Lector
    private String tipo; // Alumno o Docente
    private String nombre; // Nombre Completo del Lector
    private String usuario; // Nombre del Trabajador del Centro de Información
    private String clave; // Identificador del trabajador

    public FichaPrestamo(List<Prestamo> librosSeleccionados, String matricula, String tipo, String nombre, String usuario, String clave) {
        super(Orientation.PORTRAIT, new Format(350,500));
        author = "JHONATAN BARROSO";
        creator = "FAWARIS STORE APP";
        this.librosSeleccionados = librosSeleccionados;
        this.matricula = matricula;
        this.tipo = tipo;
        this.nombre = nombre;
        this.usuario = usuario;
        this.clave = clave;
        generarT();
    }

    @Override
    public void Footer() {}

    @Override
    public void Header() {}

    private void cabeceraT() throws IOException{

        HashSet<FontStyle> style = new HashSet();
        style.add(FontStyle.BOLD);
        this.setFont("Helvetica",null,12);
        this.Cell(100, 4, "CECyTE Plantel 25 \"San Pablo Huixtepec\"", Position.NEXTLINE, Alignment.CENTER);
        this.setFont("Helvetica",null,10);
        this.Cell(100, 4,"CENTRO DE INFORMACIÓN", Position.NEXTLINE, Alignment.CENTER);
        this.Cell(100,4,"plantel25@cecyteo.com",Position.NEXTLINE, Alignment.CENTER);
        this.Ln(10);
        this.setFont("Helvetica",style,10);
        this.Cell(100,4,"FICHA DE PRÉSTAMO",Position.NEXTLINE, Alignment.RIGHT);
        this.Cell(100,4,"Clave Trabajador: "+clave, Position.NEXTLINE, Alignment.RIGHT);
        this.Ln(15);
        this.Cell(80,4,"DATOS DE LA FICHA", Position.NEXTLINE, Alignment.LEFT);
        this.setFont("Helvetica",null, 10);
        this.Cell(80,5,"Fecha: "+new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime()),Position.NEXTLINE);
        this.Cell(80,5,"Matricula Lector: "+matricula,Position.NEXTLINE);
        this.Cell(80,5,"Nombre: "+nombre,Position.NEXTLINE);
        this.Cell(80,5,"Tipo: "+ tipo,Position.NEXTLINE);
        this.Ln(5);
        this.setFont("Helvetica", style, 8);
        this.Cell(13, 10, "ID", 0);
        this.Cell(40, 10, "Libro",Position.RIGHTOF, Alignment.LEFT);
        this.Cell(15, 10, "Cantidad",Position.RIGHTOF, Alignment.RIGHT);
        this.Cell(20, 10, "Devolución",Position.RIGHTOF, Alignment.RIGHT);
        this.Ln(7);
        this.Cell(100,0,"",new Borders(false,true,false,false),Position.BELOW, Alignment.LEFT,false, 0);

        this.Ln(7);
        
    }
    private void cuerpoT() throws IOException{
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
       this.setFont("Helvetica",null, 7);
       for(Prestamo d: librosSeleccionados){
            this.Cell(13, -5,""+d.getId());
	    this.Cell(40, -5,""+d.getIdLibro().getTitulo(),Position.RIGHTOF, Alignment.JUSTIFIED);
            this.Cell(15, -5,""+d.getCantidad(),Position.RIGHTOF, Alignment.RIGHT);
            this.Cell(20, -5,""+f.format(d.getFechaDevolucion()),Position.RIGHTOF, Alignment.RIGHT);
            this.Ln(4); 
       }
    }
    
    private void pieT() throws IOException{
        HashSet<FontStyle> style = new HashSet();
        style.add(FontStyle.BOLD);
        this.Ln(6);
        this.Cell(100,0,"",new Borders(false,true,false,false),Position.BELOW, Alignment.LEFT,false, 0);
        this.Ln(2);    
        this.setFont("Helvetica", style, 8);
        this.Cell(25, 10, "TOTAL DE LIBROS: ", 0);    
        this.Cell(20, 10, "", 0);
        this.Cell(15, 10, ""+librosSeleccionados.size(),Position.RIGHTOF, Alignment.RIGHT);  
        this.Ln(20);
        this.Cell(100,5,"\"La lectura hace al hombre completo; la conversación lo hace ágil, el escribir",Position.NEXTLINE, Alignment.CENTER);
        this.Cell(100,5,"lo hace preciso\". Francis Bacon",Position.NEXTLINE, Alignment.CENTER);
        this.Ln(10);
        this.Cell(100,0,"Regrese Pronto :)",Position.NEXTLINE, Alignment.CENTER);
    }
    private void generarT(){
        try {
            addPage();
            cabeceraT();
            cuerpoT();
            pieT();
            output(new File("Comprobante.pdf"));
        } catch (IOException ex) {
            Logger.getLogger(FichaPrestamo.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }

    public List<Prestamo> getLibrosSeleccionados() {
        return librosSeleccionados;
    }

    public void setLibrosSeleccionados(List<Prestamo> librosSeleccionados) {
        this.librosSeleccionados = librosSeleccionados;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    

    
    
    
}
