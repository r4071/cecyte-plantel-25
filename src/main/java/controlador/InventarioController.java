/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/J2EE/EJB30/StatelessEjbClass.java to edit this template
 */
package controlador;

import accesoDatos.AutorFacade;
import accesoDatos.EditorialFacade;
import accesoDatos.LibroFacade;
import archivos.InventarioCSV;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import pdf.InventarioPDF;

/**
 *
 * @author JHONATAN
 */
@Named("inventarioController")
@SessionScoped
public class InventarioController implements Serializable {

    @EJB
    private LibroFacade libroFacade;

    @EJB
    private EditorialFacade editorialFacade;

    @EJB
    private AutorFacade autorFacade;
    
    public int getNumeroLibros(){
        return libroFacade.count();
    }
    public int getNumeroEditoriales(){
        return editorialFacade.count();
    }
    public int getNumeroAutores(){
        return autorFacade.count();
    }
     public void downloadFile() throws IOException {
            
            FileInputStream fis = null;
            try {
                InventarioCSV csv = new InventarioCSV(libroFacade.findAll());
                csv.generaCSV();
                File fichero= new File("Inventario.csv");
                FacesContext ctx = FacesContext.getCurrentInstance();
                fis = new FileInputStream(fichero);
                byte[] bytes = new byte[1000];
                int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                String contentType = "text/comma-separated-values";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    fis.close();
                } catch (IOException ex) {
                    Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
}

public void downloadFilePDF() throws IOException {

        FileInputStream fis = null;
        try {
            InventarioPDF pdf = new InventarioPDF(libroFacade.findAll());
            File fichero= new File("Inventario.pdf");
            FacesContext ctx = FacesContext.getCurrentInstance();
            fis = new FileInputStream(fichero);
            byte[] bytes = new byte[1000];
            int read = 0;
            if (!ctx.getResponseComplete()) {
                String fileName = fichero.getName();
                //String contentType = "application/vnd.ms-excel";
                String contentType = "application/pdf";
                HttpServletResponse response =
                        (HttpServletResponse) ctx.getExternalContext().getResponse();
                
                response.setContentType(contentType);
                
                response.setHeader("Content-Disposition",
                        "attachment;filename=\"" + fileName + "\"");
                
                ServletOutputStream out = response.getOutputStream();
                
                while ((read = fis.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                out.flush();
                out.close();
                System.out.println("\nDescargado\n");
                ctx.responseComplete();
            }       
            } catch (FileNotFoundException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                    try {
                        fis.close();
                    } catch (IOException ex) {
                        Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
        }
    
    
    
}
