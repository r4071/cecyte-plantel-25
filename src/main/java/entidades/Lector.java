/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author JHONATAN
 */
@Entity
@Table(name = "lector")
@NamedQueries({
    @NamedQuery(name = "Lector.findAll", query = "SELECT l FROM Lector l"),
    @NamedQuery(name = "Lector.findByIdlector", query = "SELECT l FROM Lector l WHERE l.idlector = :idlector")})
public class Lector implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idlector")
    private Integer idlector;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitante")
    private List<Prestamo> prestamoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSolicitante")
    private List<Solicitud> solicitudList;

    public Lector() {
        idlector = -1;
    }

    public Lector(Integer idlector) {
        this.idlector = idlector;
    }

    public Integer getIdlector() {
        return idlector;
    }

    public void setIdlector(Integer idlector) {
        this.idlector = idlector;
    }

    public List<Prestamo> getPrestamoList() {
        return prestamoList;
    }

    public void setPrestamoList(List<Prestamo> prestamoList) {
        this.prestamoList = prestamoList;
    }

    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idlector != null ? idlector.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lector)) {
            return false;
        }
        Lector other = (Lector) object;
        if ((this.idlector == null && other.idlector != null) || (this.idlector != null && !this.idlector.equals(other.idlector))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Lector[ idlector=" + idlector + " ]";
    }
    
}
